/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coe318.lab5;

/**
 *
 * @author Adeeb Bakht
 */
import java.util.Scanner;

public class SimpleUI implements UserInterface {
    private Blackjack game;
    private Scanner user = new Scanner(System.in);

  @Override
    public void setGame(Blackjack game) {
        this.game = game;
    }

  @Override
    public void display() {
    System.out.println("House holds:\n" + this.game.getHouseCards().toString());
    System.out.println("You hold:\n" + this.game.getYourCards().toString());
        //FIX THIS
    }

  @Override
    public boolean hitMe() {
    System.out.println("Another card? (y/n)");
    String choice = user.nextLine();
    boolean hit = false;
    switch(choice){
        case "y":
        hit = true;
        break;
        case "n":
        hit = false;
        break;
        default:
        System.out.println("Select (y/n)\n");
        hitMe();
    }
    return hit;
        //FIX THIS
    }
  @Override
    public void gameOver() {
        this.display();
        int PlayerScore = game.score(game.getYourCards());
        int HouseScore = game.score(game.getHouseCards());
        System.out.println("Your Score: " + PlayerScore + ", House Score: " + HouseScore);
        if((PlayerScore > HouseScore || HouseScore > 21) && (PlayerScore <= 21)) {
            System.out.println("You win!");
        }
        else {
            System.out.println("House wins!");
        }    
        //FIX THIS
    }
